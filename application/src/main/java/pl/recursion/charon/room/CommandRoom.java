package pl.recursion.charon.room;

import pl.recursion.charon.room.boundary.valueobject.RoomID;
import pl.recursion.charon.room.confidential.DocumentDataBaseInMemory;
import pl.recursion.charon.room.confidential.PrivateRoomRepositoryImpl;
import pl.recursion.charon.room.confidential.PrivateRoomService;
import pl.recursion.charon.room.open.RelationDatabaseInMemory;
import pl.recursion.charon.user.UserFactoryOfMemory;
import pl.recursion.charon.user.boundary.valueobject.UserID;

public class CommandRoom {


    public static void main(String[] strings) {
        RelationDatabaseInMemory relationDatabaseInMemory = new RelationDatabaseInMemory();
        UserFactoryOfMemory userFactory = new UserFactoryOfMemory();
        DocumentDataBaseInMemory documentDataBaseInMemory = new DocumentDataBaseInMemory();
        RoomFactoryStrategy roomFactory = new RoomFactoryStrategy(relationDatabaseInMemory, documentDataBaseInMemory);
        PrivateRoomService privateRoomService =
                new PrivateRoomService(new PrivateRoomRepositoryImpl(new DocumentDataBaseInMemory()));
        RoomService roomService = new RoomService(roomFactory, userFactory, privateRoomService);

        roomService.enterToRoom(UserID.create("USER2"), RoomID.create("Private"));
        roomService.enterToRoom(UserID.create("USER3"), RoomID.create("Private"));

        System.out.println("====================================================");

        roomService.enterToRoom(UserID.create("USER1"), RoomID.create("ROOM1"));




        System.out.println("xxxx");

    }

}
