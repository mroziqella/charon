package pl.recursion.charon.user;

import io.vavr.control.Option;
import pl.recursion.charon.user.boundary.valueobject.UserID;

public interface UserFactory {

    Option<UserRoot> findUser(UserID userID);

}
