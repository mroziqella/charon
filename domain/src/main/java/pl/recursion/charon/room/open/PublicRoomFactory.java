package pl.recursion.charon.room.open;

import io.vavr.control.Option;
import pl.recursion.charon.room.boundary.Room;
import pl.recursion.charon.room.boundary.valueobject.RoomID;


interface PublicRoomFactory {

    Option<Room> findRoom(RoomID roomID);

}
