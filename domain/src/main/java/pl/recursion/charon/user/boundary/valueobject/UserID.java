package pl.recursion.charon.user.boundary.valueobject;

import java.util.Objects;

public class UserID {

    private final String id;

    private UserID(String id) {
        this.id = id;
    }

    public static UserID create(String id) {
        return new UserID(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final UserID userID = (UserID) o;
        return Objects.equals(id, userID.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "UserID{" +
                "id='" + id + '\'' +
                '}';
    }
}
