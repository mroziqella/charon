package pl.recursion.charon.room;

import io.vavr.control.Option;
import pl.recursion.charon.room.boundary.Room;
import pl.recursion.charon.room.boundary.valueobject.RoomID;
import pl.recursion.charon.room.confidential.DocumentDataBaseInMemory;
import pl.recursion.charon.room.confidential.PrivateRoomExtendsFactory;
import pl.recursion.charon.room.open.PublicRoomExtendsFactory;
import pl.recursion.charon.room.open.RelationDatabaseInMemory;


public class RoomFactoryStrategy implements PublicRoomExtendsFactory, PrivateRoomExtendsFactory {

    private final RelationDatabaseInMemory relationDatabaseInMemory;
    private final DocumentDataBaseInMemory documentDataBaseInMemory;

    RoomFactoryStrategy(RelationDatabaseInMemory publicRoomFactory,
                        DocumentDataBaseInMemory privateRoomFactory) {
        this.relationDatabaseInMemory = publicRoomFactory;
        this.documentDataBaseInMemory = privateRoomFactory;
    }

    @Override
    public Option<Room> findRoom(RoomID roomID) {
        if (RoomID.create("Private").equals(roomID)) {
            return documentDataBaseInMemory.findRoom(roomID);
        }
        return relationDatabaseInMemory.findRoom(roomID);
    }
}
