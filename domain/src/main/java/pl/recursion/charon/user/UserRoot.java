package pl.recursion.charon.user;

import pl.recursion.charon.user.boundary.User;
import pl.recursion.charon.user.boundary.valueobject.UserID;

public class UserRoot implements User {

    private final UserID userID;

    UserRoot(UserID userID) {
        this.userID = userID;
    }

    @Override
    public boolean equal(User banedBy) {
        return this.equals(banedBy);
    }

    @Override
    public String toString() {
        return "UserRoot{" +
                "userID=" + userID +
                '}';
    }
}
