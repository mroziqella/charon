package pl.recursion.charon.room.confidential;

import io.vavr.control.Option;
import pl.recursion.charon.room.boundary.Room;
import pl.recursion.charon.room.boundary.valueobject.RoomID;


public class DocumentDataBaseInMemory {

    public Option<Room> findRoom(RoomID roomID) {
        return Option.of(new PrivateRoomRoot(roomID, new LoggedUsers(), new Owner(), new Permissions()));

    }


    public void save(PrivateRoomRoot privateRoomRoot){
        System.out.println("Save room: ");
        System.out.println(privateRoomRoot.loggedUsers);
        System.out.println(privateRoomRoot.owner);
        System.out.println(privateRoomRoot.roomID);
        System.out.println(privateRoomRoot.permissions);
    }
}
