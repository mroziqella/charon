package pl.recursion.charon.room.open;

import io.vavr.control.Option;
import pl.recursion.charon.room.boundary.Room;
import pl.recursion.charon.room.boundary.valueobject.RoomID;


public class RelationDatabaseInMemory {


    public Option<Room> findRoom(RoomID roomID) {
        return Option.of(new PublicRoomRoot(roomID, new AnonymousUsers(), new Creator()));
    }
}
