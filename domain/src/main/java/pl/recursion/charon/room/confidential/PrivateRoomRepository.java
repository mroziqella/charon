package pl.recursion.charon.room.confidential;

import pl.recursion.charon.room.boundary.Room;

interface PrivateRoomRepository {

    void save(Room room);

}
