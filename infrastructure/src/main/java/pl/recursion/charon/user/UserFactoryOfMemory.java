package pl.recursion.charon.user;

import io.vavr.control.Option;
import pl.recursion.charon.user.boundary.valueobject.UserID;

public class UserFactoryOfMemory implements UserFactory {

    @Override
    public Option<UserRoot> findUser(UserID userID) {
        return Option.of(new UserRoot(userID));
    }
}
