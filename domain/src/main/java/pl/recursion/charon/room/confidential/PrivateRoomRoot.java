package pl.recursion.charon.room.confidential;

import io.vavr.control.Either;
import pl.recursion.charon.room.boundary.Room;
import pl.recursion.charon.room.boundary.event.AccessDenied;
import pl.recursion.charon.room.boundary.valueobject.RoomID;
import pl.recursion.charon.user.boundary.User;

class PrivateRoomRoot implements Room {

    final RoomID roomID;
    final LoggedUsers loggedUsers;
    final Owner owner;
    final Permissions permissions;


    PrivateRoomRoot(RoomID roomID, LoggedUsers loggedUsers, Owner owner, Permissions permissions) {
        this.roomID = roomID;
        this.loggedUsers = loggedUsers;
        this.owner = owner;
        this.permissions = permissions;
    }

    @Override
    public Either<AccessDenied, User> enter(User user) {
        Either<AccessDenied, User> permission = this.permissions.check(user);
        return permission.peek(loggedUsers::add);
    }

    @Override
    public void exit(User user) {
        loggedUsers.remove(user);
    }

    @Override
    public void startTransmission() {

    }

    @Override
    public void endTransmission() {

    }


}
