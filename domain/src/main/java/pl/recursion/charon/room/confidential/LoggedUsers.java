package pl.recursion.charon.room.confidential;


import pl.recursion.charon.user.boundary.User;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

class LoggedUsers {

    private final Set<User> users = new HashSet<>();

    void remove(User user) {

    }

    void add(User user) {
        this.users.add(user);
        System.out.println("Added user to private room: " + user);

    }

    @Override
    public String toString() {
        return "LoggedUsers{" +
                "users=" + users +
                '}';
    }
}
