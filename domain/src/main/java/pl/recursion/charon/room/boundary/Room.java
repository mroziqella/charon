package pl.recursion.charon.room.boundary;

import io.vavr.control.Either;
import pl.recursion.charon.room.boundary.event.AccessDenied;
import pl.recursion.charon.user.UserRoot;
import pl.recursion.charon.user.boundary.User;

public interface Room {

    Either<AccessDenied, User> enter(User user);

    void exit(User user);

    void startTransmission();

    void endTransmission();


}
