package pl.recursion.charon.room.confidential;

import io.vavr.control.Option;
import pl.recursion.charon.room.boundary.Room;
import pl.recursion.charon.room.boundary.valueobject.RoomID;


interface PrivateRoomFactory {

    Option<Room> findRoom(RoomID roomID);

}
