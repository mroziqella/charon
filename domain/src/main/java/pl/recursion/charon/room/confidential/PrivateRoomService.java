package pl.recursion.charon.room.confidential;

import pl.recursion.charon.room.boundary.Room;

public class PrivateRoomService {


    private final PrivateRoomRepository privateRoomRepository;

    public PrivateRoomService(PrivateRoomRepository privateRoomRepository) {
        this.privateRoomRepository = privateRoomRepository;
    }

    public void save(Room room) {
        System.out.println("This is service");
        privateRoomRepository.save(room);
    }

}
