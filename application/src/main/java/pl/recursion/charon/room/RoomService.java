package pl.recursion.charon.room;

import io.vavr.control.Option;
import pl.recursion.charon.room.boundary.Room;
import pl.recursion.charon.room.boundary.valueobject.RoomID;
import pl.recursion.charon.room.confidential.PrivateRoomService;
import pl.recursion.charon.user.UserFactory;
import pl.recursion.charon.user.UserRoot;
import pl.recursion.charon.user.boundary.valueobject.UserID;

public class RoomService {
    private final RoomFactoryStrategy roomFactoryStrategy;
    private final UserFactory userFactory;
    private final PrivateRoomService privateRoomService;


    public RoomService(RoomFactoryStrategy roomFactory, UserFactory userFactory, PrivateRoomService privateRoomService) {
        this.roomFactoryStrategy = roomFactory;
        this.userFactory = userFactory;
        this.privateRoomService = privateRoomService;
    }

    public void enterToRoom(UserID userID, RoomID roomID) {
        Option<Room> roomRoot = roomFactoryStrategy.findRoom(roomID);
        Option<UserRoot> user = userFactory.findUser(userID);

        roomRoot.flatMap(room -> user.map(room::enter))
                .getOrElseThrow(() -> new IllegalArgumentException("Incorrect data"))
                .peekLeft(System.out::println);


        roomRoot.peek(privateRoomService::save);

    }

}
