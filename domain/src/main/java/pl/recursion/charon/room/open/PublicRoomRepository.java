package pl.recursion.charon.room.open;

import pl.recursion.charon.room.boundary.Room;

interface PublicRoomRepository {

    void save(Room room);

}
