package pl.recursion.charon.room.open;

import io.vavr.control.Either;
import pl.recursion.charon.room.boundary.Room;
import pl.recursion.charon.room.boundary.event.AccessDenied;
import pl.recursion.charon.room.boundary.valueobject.RoomID;
import pl.recursion.charon.user.boundary.User;

class PublicRoomRoot implements Room {

    private final RoomID roomID;
    private final AnonymousUsers anonymousUsers;
    private final Creator creator;


    public PublicRoomRoot(RoomID roomID, AnonymousUsers anonymousUsers, Creator creator) {
        this.roomID = roomID;
        this.anonymousUsers = anonymousUsers;
        this.creator = creator;
    }

    @Override
    public Either<AccessDenied, User> enter(User user) {
        anonymousUsers.add(user);
        return Either.right(user);
    }

    @Override
    public void exit(User user) {
        anonymousUsers.remove(user);
    }

    @Override
    public void startTransmission() {

    }

    @Override
    public void endTransmission() {

    }

}
