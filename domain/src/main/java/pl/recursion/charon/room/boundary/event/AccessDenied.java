package pl.recursion.charon.room.boundary.event;

import java.util.Objects;

public class AccessDenied {

    private final String reason;

    public AccessDenied(String reason) {
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final AccessDenied that = (AccessDenied) o;
        return Objects.equals(reason, that.reason);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reason);
    }

    @Override
    public String toString() {
        return "Access denied reason: " + reason;
    }
}
