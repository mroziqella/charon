package pl.recursion.charon.user.boundary;

public interface User {


    default boolean equal(User banedBy){
        return this.equals(banedBy);
    };
}
