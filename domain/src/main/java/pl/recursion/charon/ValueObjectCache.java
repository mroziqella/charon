package pl.recursion.charon;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Optional;
import java.util.WeakHashMap;

public class ValueObjectCache {
    private final static Map<Object, WeakReference<Object>> CACHE = new WeakHashMap<>();


    public static <T> Optional<T> get(T obj) {
        WeakReference<Object> objectWeakReference = CACHE.get(obj);
        if (objectWeakReference == null) {
            return Optional.empty();
        }
        Object o = objectWeakReference.get();
        return Optional.ofNullable((T)o);

    }

    public static <T> T add(T obj) {
        CACHE.put(obj, new WeakReference<>(obj));
        return obj;
    }

}
