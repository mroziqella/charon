package pl.recursion.charon.room.confidential;

import io.vavr.control.Either;
import pl.recursion.charon.room.boundary.event.AccessDenied;
import pl.recursion.charon.user.boundary.User;

class Permissions {
    Either<AccessDenied, User> check(User user) {

        return Either.right(user);
    }
}
