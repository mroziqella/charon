package pl.recursion.charon.room.confidential;

import pl.recursion.charon.room.boundary.Room;


public class PrivateRoomRepositoryImpl implements PrivateRoomRepository {

    private final DocumentDataBaseInMemory documentDataBaseInMemory;

    public PrivateRoomRepositoryImpl(DocumentDataBaseInMemory documentDataBaseInMemory) {
        this.documentDataBaseInMemory = documentDataBaseInMemory;
    }

    @Override
    public void save(Room room) {
        if (room instanceof PrivateRoomRoot) {
            documentDataBaseInMemory.save((PrivateRoomRoot) room);
        }
        else {
            System.out.println("Public room  cannot persistent");
        }
    }

}
