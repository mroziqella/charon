package pl.recursion.charon.room.boundary.valueobject;

import pl.recursion.charon.ValueObjectCache;

import java.util.Objects;

public class RoomID {

    private final String uuid;

    private RoomID(String uuid) {
        this.uuid = uuid;
    }

    public static RoomID create(String uuid) {
        RoomID roomID = new RoomID(uuid);
        return ValueObjectCache.get(roomID)
                .orElseGet(()-> ValueObjectCache.add(roomID));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final RoomID roomID = (RoomID) o;
        return Objects.equals(uuid, roomID.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
